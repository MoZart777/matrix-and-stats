package com.mozart.math.matrixandstats;

import com.mozart.math.matrixandstats.exceptions.MatrixException;
import com.mozart.math.matrixandstats.matrix.Matrix;
import com.mozart.math.matrixandstats.matrix.MatrixUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.mozart.math.matrixandstats.matrix.MatrixUtils.matrixToString;

/**
 * <p>
 * Test for matrix
 * </p>
 * miet-project. Created: 16.09.2018<br/>
 * Copyright 2018 by MoZart <br/>
 * All rights reserved.<br/>
 */
public class MatrixTest {

    @BeforeClass
    public void init() {
        MatrixUtils.setIndexStr("3");
    }

    private static Double[][] square6ValidMatrix() {
        return new Double[][]{
                {5.0, 7.0, 6.0, 9.0, -2.0, 8.0},
                {-6.0, 5.0, 9.0, -4.0, 0.0, 0.0},
                {6.0, -4.0, 7.0, 12.0, 6.0, -9.0},
                {4.0, 7.0, 6.0, 3.0, 9.0, 8.0},
                {4.0, 6.0, -3.0, -5.0, -7.0, 9.0},
                {5.0, 8.0, 3.0, 6.0, 2.0, 1.0}};
    }

    private static Double[][] notSquare3x6Matrix() {
        return new Double[][]{
                {1.0, 2.0, 3.0},
                {4.0, 5.0, 6.0},
                {7.0, 8.0, 9.0},
                {10.0, 11.0, 12.0},
                {13.0, 14.0, 15.0},
                {16.0, 17.0, 18.0}};
    }

    @Test
    public void detMartix() {
        Assert.assertEquals(new Matrix(square6ValidMatrix()).getDet(), -1085485.0);
    }

    @Test(expectedExceptions = MatrixException.class)
    public void detMartixWithException() {
        new Matrix(notSquare3x6Matrix()).getDet();
    }

    @Test
    public void transporatedMartix() {
        Double[][] res = {
                {1.0, 4.0, 7.0, 10.0, 13.0, 16.0},
                {2.0, 5.0, 8.0, 11.0, 14.0, 17.0},
                {3.0, 6.0, 9.0, 12.0, 15.0, 18.0}};
        Assert.assertEquals(
                matrixToString(new Matrix(notSquare3x6Matrix()).getTransporatedMartix()),
                matrixToString(res));
    }

    @Test
    public void revertMatrix() {
        Double[][] mat1 = {
                {1.0, 2.0, 2.0},
                {3.0, -2.0, 1.0},
                {4.0, 2.0, 2.0}};
        Double[][] res1 = {
                {-0.333, -0.0, 0.333},
                {-0.111, -0.333, 0.278},
                {0.778, 0.333, -0.444}};
        Assert.assertEquals(
                matrixToString(new Matrix(mat1).getRevertMatrix()),
                matrixToString(res1));
    }

    @Test(expectedExceptions = MatrixException.class)
    public void revertMatrixWithExceptionSize() {
        Double[][] in = {
                {2.0, 1.0, 3.0},
                {7.0, 2.0, 9.0}};
        Matrix matrix = new Matrix(in);
        matrix.getRevertMatrix();
    }

    @Test(expectedExceptions = MatrixException.class)
    public void revertMatrixWithExceptionDet() {
        Double[][] in = {
                {1.0, 2.0, 3.0},
                {4.0, 5.0, 6.0},
                {7.0, 8.0, 9.0}};
        Matrix matrix = new Matrix(in);
        matrix.getRevertMatrix();
    }

    @Test
    public void multiplicationMatrix() {
        Double[][] in1 = {
                {-1.0, 2.0, 0.0},
                {1.0, 0.0, 3.0}};
        Double[][] in2 = {
                {3.0, 0.0},
                {1.0, -1.0},
                {2.0, 4.0}};
        Double[][] etalon = {
                {-1.0, -2.0},
                {9.0, 12.0}};
        Matrix matrix = new Matrix(in1);
        Matrix matrix2 = new Matrix(in2);
        Assert.assertEquals(
                matrixToString(matrix.multiplication(matrix2)),
                matrixToString(etalon));
        Double[][] in5 = {
                {1.0, 2.0, 3.0, 4.0},
                {5.0, 6.0, 7.0, 8.0},
                {9.0, 10.0, 11.0, 12.0}};
        Matrix matrix5 = new Matrix(in5);
        Double[][] in6 = {
                {6.0, -6.0, 5.0},
                {-5.0, 4.0, -4.0},
                {3.0, -3.0, 2.0},
                {-2.0, 1.0, -1.0}};
        Matrix matrix6 = new Matrix(in6);
        Double[][] etalon3 = {
                {-3.0, -3.0, -1.0},
                {5.0, -19.0, 7.0},
                {13.0, -35.0, 15.0}};
        Assert.assertEquals(
                matrixToString(matrix5.multiplication(matrix6)),
                matrixToString(etalon3));
    }

    @Test(expectedExceptions = MatrixException.class)
    public void multiplicationMatrixWithException() {
        Matrix matrix = new Matrix(new Double[][]{{3.0}, {1.0}});
        new Matrix(square6ValidMatrix()).multiplication(matrix);
    }
}
