package com.mozart.math.matrixandstats;

import com.mozart.math.matrixandstats.exceptions.MatStatsException;
import com.mozart.math.matrixandstats.matstat.MatStatRandomVariable;
import com.mozart.math.matrixandstats.matstat.MatStatRandomVariables;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * <p>
 * Mat stat tests
 * </p>
 * miet-project. Created: 16.09.2018<br/>
 * Copyright 2018 by MoZart <br/>
 * All rights reserved.<br/>
 */
public class MatStatRandomVariableTest {

    @Test
    public void correctTest() {
        Double[] randomVars = {-1.0, 2.0, 5.0, 10.0, 20.0};
        Double[] p = {0.1, 0.2, 0.3, 0.3, 0.1};
        MatStatRandomVariable matStatRandomVariable = new MatStatRandomVariable(randomVars, p);
        Assert.assertEquals(matStatRandomVariable.getMathExpectation(), 6.8);
        Assert.assertEquals(matStatRandomVariable.getDispersion(), 32.16);
    }

    @Test(expectedExceptions = MatStatsException.class)
    public void errorSize() {
        Double[] randomVars = {-1.0, 2.0, 5.0, 10.0};
        Double[] p = {0.1, 0.2, 0.3, 0.3, 0.1};
        new MatStatRandomVariable(randomVars, p);
    }

    @Test(expectedExceptions = MatStatsException.class)
    public void errorP() {
        Double[] randomVars = {-1.0, 2.0, 5.0, 10.0};
        Double[] p = {0.1, 0.2, 0.3, 0.3};
        new MatStatRandomVariable(randomVars, p);
    }

    @Test
    public void correlationTest() {
        Double[] x = {25.2, 26.4, 26.0, 25.8, 24.9, 25.7, 25.7, 25.7, 26.1, 25.8, 25.9, 26.2, 25.6,
                25.4, 26.6, 26.2, 26.0, 22.1, 25.9, 25.8, 25.9, 26.3, 26.1, 26.0, 26.4, 25.8};
        Double[] y = {30.8, 29.4, 30.2, 30.5, 31.4, 30.3, 30.4, 30.5, 29.9, 30.4, 30.3, 30.5, 30.6,
                31.0, 29.6, 30.4, 30.7, 31.6, 30.5, 30.6, 30.7, 30.1, 30.6, 30.5, 30.7, 30.8};
        MatStatRandomVariables matStatRandomVariables = new MatStatRandomVariables(x, y);
        Assert.assertEquals(
                String.format("%.7f", matStatRandomVariables.getCorrelation()),
                String.format("%.7f", -0.720279034924693));
    }

    @Test(expectedExceptions = MatStatsException.class)
    public void correlationTestWithException() {
        Double[] x = {25.2, 26.4, 26.0, 25.8, 24.9, 25.7, 25.7, 25.7, 26.1, 25.8, 25.9, 26.2, 25.6,
                25.4, 26.6, 26.2, 26.0, 22.1, 25.9, 25.8, 25.9, 26.3, 26.1, 26.0, 26.4, 25.8};
        Double[] y = {30.8, 29.4, 30.2, 30.5, 31.4, 30.3, 30.4, 30.5, 29.9, 30.4, 30.3, 30.5, 30.6,
                31.0, 29.6, 30.4, 30.7, 31.6, 30.5, 30.6, 30.7, 30.1, 30.6, 30.5, 30.7};
        new MatStatRandomVariables(x, y);
    }
}