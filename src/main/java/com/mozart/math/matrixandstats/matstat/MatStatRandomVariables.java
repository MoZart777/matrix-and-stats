package com.mozart.math.matrixandstats.matstat;

import com.mozart.math.matrixandstats.exceptions.MatStatsException;
import lombok.SneakyThrows;

/**
 * <p>
 * Mat stats with 2 random variables
 * </p>
 * miet-project. Created: 16.09.2018<br/>
 * Copyright 2018 by MoZart <br/>
 * All rights reserved.<br/>
 */
public class MatStatRandomVariables {

    private final Double[] x;
    private final Double[] y;
    private final int N;

    @SneakyThrows(MatStatsException.class)
    public MatStatRandomVariables(Double[] x, Double[] y) {
        if (x == null || y == null || x.length != y.length) {
            throw MatStatsException.invalidInputData("неверные данные");
        }
        this.x = x;
        this.y = y;
        this.N = x.length;
    }

    public double getCorrelation() {
        double M_xy = 0.0;
        double M_x = 0.0;
        double M_y = 0.0;
        double S_x_qrt = 0.0;
        double S_y_qrt = 0.0;
        for (int i = 0; i < N; i++) {
            M_xy += x[i] * y[i];
            M_x += x[i];
            M_y += y[i];
            S_x_qrt += x[i] * x[i];
            S_y_qrt += y[i] * y[i];
        }
        M_xy = M_xy / N;
        M_x = M_x / N;
        M_y = M_y / N;

        S_x_qrt = S_x_qrt / N - M_x * M_x;
        S_y_qrt = S_y_qrt / N - M_y * M_y;
        return (M_xy - M_x * M_y) / (Math.pow(S_x_qrt * S_y_qrt, 0.5));
    }
}
