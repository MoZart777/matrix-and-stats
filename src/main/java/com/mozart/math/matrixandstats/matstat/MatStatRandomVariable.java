package com.mozart.math.matrixandstats.matstat;

import com.mozart.math.matrixandstats.exceptions.MatStatsException;
import lombok.SneakyThrows;

import java.util.Arrays;

/**
 * <p>
 * Mat statistics
 * </p>
 * miet-project. Created: 16.09.2018<br/>
 * Copyright 2018 by MoZart <br/>
 * All rights reserved.<br/>
 */
public class MatStatRandomVariable {

    private Double[] randomVars;
    private Double[] p;

    @SneakyThrows(MatStatsException.class)
    public MatStatRandomVariable(Double[] randomVars, Double[] p) {
        if (randomVars == null || p == null || randomVars.length == 0 || randomVars.length != p.length) {
            throw MatStatsException.invalidInputData("размерности не совпадают");
        }
        if (Arrays.stream(p).mapToDouble(d -> d).sum() != 1) {
            throw MatStatsException.invalidInputData("сумма вероятностей должна быть равна 1");
        }
        this.randomVars = randomVars;
        this.p = p;
    }

    public double getMathExpectation() {
        double m = 0.0;
        for (int i = 0; i < randomVars.length; i++) {
            m += randomVars[i] * p[i];
        }
        return m;
    }

    public double getDispersion() {
        final double M = getMathExpectation();
        double d = 0.0;
        for (int i = 0; i < randomVars.length; i++) {
            d += (randomVars[i] - M) * (randomVars[i] - M) * p[i];
        }
        return d;
    }
}
