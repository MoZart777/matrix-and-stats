package com.mozart.math.matrixandstats.exceptions;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * Mat stats exceptions static
 * </p>
 * miet-project. Created: 16.09.2018<br/>
 * Copyright 2018 by MoZart <br/>
 * All rights reserved.<br/>
 */
public class MatStatsException extends RuntimeException {

    private MatStatsException(String message) {
        super(message);
    }

    public static MatStatsException invalidInputData(@NotNull String message) {
        return new MatStatsException("Неверные данные - " + message);
    }
}
