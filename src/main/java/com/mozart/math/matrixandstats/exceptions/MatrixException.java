package com.mozart.math.matrixandstats.exceptions;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * Matrix Exceptions static
 * </p>
 * miet-project. Created: 15.09.2018<br/>
 * Copyright 2018 by MoZart <br/>
 * All rights reserved.<br/>
 */
public class MatrixException extends RuntimeException {

    private MatrixException(String message) {
        super(message);
    }

    public static MatrixException invalidInputData() {
        return new MatrixException("Неверные данные");
    }

    public static MatrixException notSquareMatrix() {
        return new MatrixException("Матрица должна быть квадратной");
    }

    public static MatrixException noRevertMatrix(@NotNull String message) {
        return new MatrixException("Обратная матрица не существует - " + message);
    }

    public static MatrixException invalidSizeMatrix(@NotNull String message) {
        return new MatrixException("Операция невозможна - " + message);
    }
}
