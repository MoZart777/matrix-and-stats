package com.mozart.math.matrixandstats.matrix;

import com.mozart.math.matrixandstats.exceptions.MatrixException;
import lombok.SneakyThrows;

import static com.mozart.math.matrixandstats.matrix.MatrixUtils.checkSquareMatrix;

/**
 * <p>
 * Matrix and its operations
 * </p>
 * miet-project. Created: 15.09.2018<br/>
 * Copyright 2018 by MoZart <br/>
 * All rights reserved.<br/>
 */
public class Matrix {

    private final Double[][] matrix;
    private final int N;
    private final int M;

    @SneakyThrows(MatrixException.class)
    public Matrix(Double[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            throw MatrixException.invalidInputData();
        }
        this.matrix = matrix;
        this.N = matrix[0].length;
        this.M = matrix.length;
    }

    @SneakyThrows(MatrixException.class)
    public double getDet() {
        checkSquareMatrix(N, M);
        return det(matrix, N);
    }

    @SneakyThrows(MatrixException.class)
    public Double[][] getTransporatedMartix() {
        Double[][] transportedMatrix = new Double[N][M];
        for (int x = 0; x < N; x++) {
            for (int y = 0; y < M; y++) {
                transportedMatrix[x][y] = matrix[y][x];
            }
        }
        return transportedMatrix;
    }

    @SneakyThrows(MatrixException.class)
    public Double[][] getRevertMatrix() {
        checkSquareMatrix(N, M);
        double det = getDet();
        if (det == 0) {
            throw MatrixException.noRevertMatrix("детерминант равен нулю");
        }
        Double[][] minorTransportedMatrix = new Double[N][M];
        if (N == 2) {
            minorTransportedMatrix[0][0] = matrix[1][1] / det;
            minorTransportedMatrix[1][1] = matrix[0][0] / det;
            minorTransportedMatrix[1][0] = -matrix[1][0] / det;
            minorTransportedMatrix[0][1] = -matrix[0][1] / det;
        } else {
            for (int x = 0; x < N; x++) {
                for (int y = 0; y < M; y++) {
                    minorTransportedMatrix[y][x] = ((x + y) % 2 == 0 ?
                            det(createMinorMatrix(matrix, x, y), N - 1) :
                            -det(createMinorMatrix(matrix, x, y), N - 1)) / det;
                }
            }
        }
        return minorTransportedMatrix;
    }

    @SneakyThrows(MatrixException.class)
    public Double[][] sum(Matrix matrix2) {
        checkingForSumAndDifference(matrix2);
        Double[][] mat2 = matrix2.getMatrix();
        Double[][] res = new Double[N][M];
        for (int x = 0; x < N; x++) {
            for (int y = 0; y < M; y++) {
                res[x][y] = matrix[x][y] + mat2[x][y];
            }
        }
        return res;
    }

    @SneakyThrows(MatrixException.class)
    public Double[][] difference(Matrix matrix2) {
        checkingForSumAndDifference(matrix2);
        Double[][] mat2 = matrix2.getMatrix();
        Double[][] res = new Double[N][M];
        for (int x = 0; x < N; x++) {
            for (int y = 0; y < M; y++) {
                res[x][y] = matrix[x][y] - mat2[x][y];
            }
        }
        return res;
    }

    @SneakyThrows(MatrixException.class)
    public Double[][] multiplication(Matrix matrix2) {
        checkingForMultiplication(matrix2);
        Double[][] mat2 = matrix2.getMatrix();
        Double[][] res = new Double[matrix2.N][M];
        for (int y = 0; y < M; y++) {
            for (int x = 0; x < matrix2.N; x++) {
                Double sum = 0.0;
                for (int k = 0; k < matrix2.M; k++) {
                    sum += matrix[y][k] * mat2[k][x];
                }
                res[y][x] = sum;
            }
        }
        return res;
    }

    @SneakyThrows(MatrixException.class)
    public Double[][] multiplication(Double k) {
        if (k == null) {
            throw MatrixException.invalidInputData();
        }
        Double[][] mat = matrix.clone();
        for (int x = 0; x < N; x++) {
            for (int y = 0; y < M; y++) {
                mat[x][y] *= k;
            }
        }
        return mat;
    }

    @SneakyThrows(MatrixException.class)
    public Double[][] division(Matrix matrix2) {
        if (matrix2 == null) {
            throw MatrixException.invalidInputData();
        }
        Matrix matrix2New = new Matrix(matrix2.getRevertMatrix());
        checkingForMultiplication(matrix2New);
        return multiplication(matrix2New);
    }

    @SneakyThrows(MatrixException.class)
    public Double[][] division(Double k) {
        if (k == null || k == 0) {
            throw MatrixException.invalidInputData();
        }
        return multiplication(1 / k);
    }

    private static double det(Double[][] mat, int n) {
        if (n == 2) {
            return mat[0][0] * mat[1][1] - mat[1][0] * mat[0][1];
        }
        double res = 0.0;
        for (int i = 0; i < n; i++) {
            res += mat[i][0] * (i % 2 == 0 ?
                    det(createMinorMatrix(mat, i, 0), n - 1) :
                    -det(createMinorMatrix(mat, i, 0), n - 1));
        }
        return res;
    }

    private static Double[][] createMinorMatrix(Double[][] mat, final int excludeX, final int excludeY) {
        int length = mat.length;
        Double[][] res = new Double[length - 1][length - 1];
        int currentX = 0;
        for (int x = 0; x < length; x++) {
            if (x != excludeX) {
                int currentY = 0;
                for (int y = 0; y < length; y++) {
                    if (y != excludeY) {
                        res[currentX][currentY++] = mat[x][y];
                    }
                }
                currentX++;
            }
        }
        return res;
    }

    @SneakyThrows(MatrixException.class)
    private void checkingForSumAndDifference(Matrix matrix) {
        if (matrix == null || (N != matrix.N || M != matrix.M)) {
            throw MatrixException.invalidSizeMatrix("некорректные размерности матриц");
        }
    }

    @SneakyThrows(MatrixException.class)
    private void checkingForMultiplication(Matrix matrix) {
        if (matrix == null || (N != matrix.M)) {
            throw MatrixException.invalidSizeMatrix("некорректные размерности матриц");
        }
    }

    private Double[][] getMatrix() {
        return matrix;
    }
}
