package com.mozart.math.matrixandstats.matrix;

import com.mozart.math.matrixandstats.exceptions.MatrixException;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * Utils for matrix operations
 * </p>
 * miet-project. Created: 16.09.2018<br/>
 * Copyright 2018 by MoZart <br/>
 * All rights reserved.<br/>
 */
public class MatrixUtils {

    private static String indexStr;

    public static String matrixToString(Double[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return StringUtils.EMPTY;
        }
        int index = indexStr != null ? Integer.valueOf(indexStr) : 2;
        final int maxDigit = maxLength(matrix);
        StringBuilder res = new StringBuilder("\n");
        for (int x = 0; x < matrix.length; x++) {
            res.append("\t");
            for (int y = 0; y < matrix[0].length; y++) {
                double val = matrix[x][y];
                res.append(spaces(maxDigit - length(val)))
                        .append(String.format("%" + "." + index + "f", val))
                        .append("  ");
            }
            res.append("\n");
        }
        return res.toString();
    }

    private static String spaces(@NotNull int num) {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < num; i++) {
            res.append(" ");
        }
        return res.toString();
    }

    private static int maxLength(@NotNull final Double[][] matrix) {
        double max = matrix[0][0];
        double min = matrix[0][0];
        for (Double[] yArr : matrix) {
            for (Double digit : yArr) {
                if (digit > max) {
                    max = digit;
                }
                if (digit < min) {
                    min = digit;
                }
            }
        }
        int maxLen = length(max);
        int minLen = length(min);
        return maxLen > minLen ? maxLen : minLen;
    }

    private static int length(@NotNull double max) {
        return StringUtils.substringBefore(String.valueOf(max), ".").length();
    }

    @SneakyThrows(MatrixException.class)
    static void checkSquareMatrix(@NotNull int x, @NotNull int y) {
        if (x != y) {
            throw MatrixException.notSquareMatrix();
        }
    }

    public static void setIndexStr(String indexStr) {
        MatrixUtils.indexStr = indexStr;
    }
}
